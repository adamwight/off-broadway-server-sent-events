defmodule OffBroadway.ServerSentEvents.Acknowledger do
  @moduledoc """
  Tracks the received event IDs as an ordered set in an ETS table, marks those
  which have been processed, and can provide an overall Last-Event-ID which is
  calculated as the oldest event ID to have been fully processed.

  Heavily inspired by OffBroadway.Kafka.Acknowledger
  """
  @behaviour Broadway.Acknowledger
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init(_opts) do
    {:ok, agent_pid} = Agent.start_link(fn -> nil end)

    state = %{
      agent_pid: agent_pid,
      table: :ets.new(nil, [:ordered_set])
    }

    {:ok, state}
  end

  @doc """
  Add unacknowledged message IDs
  """
  def add(pid, ids) do
    GenServer.cast(pid, {:add, ids})
  end

  @impl Broadway.Acknowledger
  def ack(%{pid: pid} = _ack_ref, successful, failed) do
    # TODO: Provide a path for handling failed messages.

    ids =
      successful
      |> Enum.concat(failed)
      |> Enum.map(fn %{data: %{id: id}} -> id end)
      |> Enum.filter(fn id -> id != nil end)

    GenServer.call(pid, {:ack, ids})
  end

  @impl GenServer
  def handle_cast({:add, ids}, state) do
    ids
    |> Enum.filter(fn id -> id != nil end)
    Enum.each(ids, fn id -> :ets.insert(state.table, {id, false}) end)

    {:noreply, state}
  end

  @impl GenServer
  def handle_call({:ack, ids}, _from, state) do
    Enum.each(ids, fn id ->
      :ets.insert(state.table, {id, true})
    end)

    case get_offset_to_ack(state.table) do
      nil ->
        nil

      id ->
        set_last_event_id(id, state.agent_pid)
    end

    {:reply, :ok, state}
  end

  @impl GenServer
  def handle_call({:get_last_event_id}, _from, state) do
    id = Agent.get(state.agent_pid, fn id -> id end)
    {:reply, id, state}
  end

  # Get the oldest acknowledged ID, not preceded by any unacknowledged messages.
  defp get_offset_to_ack(table, previous \\ nil) do
    oldest = :ets.first(table)

    case :ets.lookup(table, oldest) do
      [{^oldest, true}] ->
        :ets.delete(table, oldest)
        get_offset_to_ack(table, oldest)

      _ ->
        previous
    end
  end

  defp set_last_event_id(id, agent_pid) do
    Agent.update(agent_pid, fn -> id end)
  end

  def get_last_event_id(pid) do
    GenServer.call(pid, {:get_last_event_id})
  end
end
