defmodule OffBroadway.ServerSentEvents.Listener do
  use Supervisor

  def start_link(args) do
    endpoint = Keyword.fetch!(args, :endpoint)
    stream_to = Keyword.fetch!(args, :stream_to)

    children = [
      %{
        # TODO: Make it possible for multiple processes to listen to the same stream.
        id: {EventsourceEx, endpoint, stream_to},
        start: {EventsourceEx, :new, [endpoint, [stream_to: stream_to]]}
      }
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end

  # TODO: Interface functions to tear down the connection when back pressure is
  # too great, and reconnect later with an appropriate Last-Event-ID.

  @impl Supervisor
  def init(_) do
    {:ok, {}}
  end
end
