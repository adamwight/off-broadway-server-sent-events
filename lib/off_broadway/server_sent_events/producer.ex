defmodule OffBroadway.ServerSentEvents.Producer do
  @moduledoc """
  Broadway producer for Server-sent Events

  See https://html.spec.whatwg.org/multipage/server-sent-events.html

  Example consumer:

  ```elixir
  defmodule RevisionConsumerApp do
    @test_endpoint "https://stream.wikimedia.org/v2/stream/revision-create"
    use Broadway

    def start_link(_) do
      Broadway.start_link(__MODULE__,
        name: __MODULE__,
        producer: [
          module: {OffBroadway.ServerSentEvents.Producer, [endpoint: @test_endpoint]}
        ],
        processors: [
          default: [concurrency: 1]
        ],
        batchers: [
          default: [concurrency: 1, batch_size: 10]
        ]
      )
    end

    @impl Broadway
    def handle_message(_, message, _) do
      message
    end

    @impl Broadway
    def handle_batch(_, messages, _, _) do
      messages
      |> Enum.map(&Map.get(&1, :data))
      |> IO.inspect()

      messages
    end
  end

  RevisionConsumerApp.start_link({})
  :timer.sleep(5000)
  ```
  """
  use GenStage

  alias OffBroadway.ServerSentEvents.Acknowledger
  alias OffBroadway.ServerSentEvents.Listener

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(opts) do
    GenStage.start_link(__MODULE__, opts)
  end

  @impl GenStage
  def init(args) do
    # TODO: Introduce a supervisor for the children.
    {:ok, _} = Listener.start_link(Keyword.put(args, :stream_to, self()))
    # TODO: persistent acknowledger per-stream, and use to reconnect with existing cursors
    {:ok, ack_pid} = Acknowledger.start_link([])

    state = %{
      ack_pid: ack_pid
    }

    {:producer, state}
  end

  @impl GenStage
  def handle_demand(demand, state) when demand > 0 do
    # TODO: "buffer demand" as well
    {:noreply, [], state}
  end

  # Receive from source
  @impl GenStage
  def handle_info(event, state) do
    message = to_message(event, state.ack_pid)
    Acknowledger.add(state.ack_pid, [event.id])
    {:noreply, [message], state}
  end

  defp to_message(%EventsourceEx.Message{} = event, ack_pid) do
    %Broadway.Message{
      data: event,
      acknowledger: {Acknowledger, %{pid: ack_pid}, nil}
    }
  end
end
